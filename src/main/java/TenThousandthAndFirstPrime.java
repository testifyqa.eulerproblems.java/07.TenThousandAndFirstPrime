import org.junit.Assert;

public class TenThousandthAndFirstPrime {

    public static void main(String[] args) {
        tests(6, 13);
        tests(10001, 104743);
        tests(100001, 1299721);
    }

    private static long nthPrime(long n) {
        int numOfPrimes = 0;
        long prime = 1;

        while (numOfPrimes < n) {
            prime++;
            if (isPrime(prime)) {
                numOfPrimes++;
            }
        }
        System.out.println("\nThe " + n + "th prime number is: " + prime);

        return prime;
    }

    private static boolean isPrime(long n) {
        if (n < 2) return false;
        else if (n < 3) return true;

        for (int i = 2; i < Math.sqrt(n) + 1; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    private static void tests(long n, int prime) {
        Assert.assertEquals(nthPrime(n), prime);
    }
}
